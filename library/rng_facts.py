#!/usr/bin/python
#coding: utf-8 -*-

# (c) 2016, Michael Scherer <mscherer@redhat.com>
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this software.  If not, see <http://www.gnu.org/licenses/>.

DOCUMENTATION = '''
---
module: rng_facts
short_description:
version_added:
options:
'''

EXAMPLES = '''
- name: Gather information about a system
  rng_facts:

- name: Display the rng device
  debug: msg="Realm is {{ ansible_rng_device }}"
'''

RETURN = '''
rng_device:
    description: rng device of the system
    type: string
    returned: always
'''


def main():
    module = AnsibleModule(
        argument_spec=dict(
        ),
    )

    device_found = ''
    for prefix in ['/dev/', '/dev/misc/']:
        for d in ['hwrng',
                  'hw_random',
                  'hwrandom',
                  'intel_rng',
                  'i810_rng']:
            device = prefix + d
            if os.path.exists(device):
                if stat.S_ISCHR(os.stat(device).st_mode):
                    device_found = device

    ansible_facts = {}
    if device_found != '':
        ansible_facts['rng_device'] = device_found
    module.exit_json(changed=False, ansible_facts=ansible_facts)

# this is magic, see lib/ansible/module_common.py
from ansible.module_utils.basic import *
main()
